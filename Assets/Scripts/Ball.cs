﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour 
{
    public int TeamId { get; private set; }

    Rigidbody m_rigidBody;

	// Use this for initialization
	void Start () 
    {
        TeamId = -1;

        m_rigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
    {

        if (m_rigidBody.position.y < -100.0f)
        {
            Destroy(this.gameObject);
        }
	}

    void OnCollisionEnter(Collision c)
    {
        var t = c.collider.GetComponent<Tile>();
        if (t != null)
        {
            if (TeamId != t.m_team)
            {
                TeamId = t.m_team;
                Debug.Log("Ball -> " + TeamId);
            }
        }
    }
}
