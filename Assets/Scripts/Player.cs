﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    public Controller m_controller;

    public Hook m_hookPrefab;

    public float m_moveSpeed = 5.0f;
    public float m_moveForce = 5.0f;
    public float m_jumpForce = 5.0f;
    public float m_airControl = 0.2f;

    public float m_hookSpeed = 25.0f;
    public float m_hookLifetime = 1.0f;
    public float m_hookMinPower = 10.0f;
    public float m_hookMaxPower = 10.0f;
    public float m_chargeRate = 1.0f;

    public event System.Action<Player> OnDeath;

    public int Id { get; set; }

    public GameObject m_cutie;

    public Transform m_chargeIndicator;

    Rigidbody m_rigidBody;
    IsOnGround m_isOnGround;
    Hook m_hook;
    float m_knockoutTimer = 0.0f;

    float m_charge = 0.0f;
    enum ChargeState
    { 
        NotCharging,
        Push,
        Pull
    }
    ChargeState m_chargeState = ChargeState.NotCharging;

	void Start () 
    {
        m_rigidBody = GetComponent<Rigidbody>();
        m_isOnGround = GetComponentInChildren<IsOnGround>();
        GetComponent<Grappable>().Id = Id;
	}

    public void SetMaterial(Material mat)
    {
        m_cutie.GetComponent<SkinnedMeshRenderer>().material = mat;
    }
	
	void Update () 
    {
        float knockFactor = 1.0f;

        m_knockoutTimer -= Time.deltaTime;

        if (m_knockoutTimer > 0.0f)
        {
            knockFactor = 0.0f;
        }

        Vector3 vel = m_rigidBody.velocity;

        Vector2 move = m_controller.GetJoystick(Xbox360ControllerJoystickId.Left);
        Vector2 targetSpeed = move * m_moveSpeed;

        Vector2 force = knockFactor * m_moveForce * (targetSpeed - new Vector2(vel.x, vel.z)) * (m_isOnGround.OnGround ? 1.0f : m_airControl);

        m_rigidBody.AddForce(new Vector3(force.x, 0, force.y));

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.A) && m_isOnGround.OnGround)
        {
            m_rigidBody.AddForce(Vector3.up * m_jumpForce, ForceMode.Impulse);
        }

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.RB))
        {
            if (m_chargeState == ChargeState.NotCharging)
            {
                m_chargeState = ChargeState.Pull;
                m_charge = 0.0f;
            }
        }

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.LB))
        {
            if (m_chargeState == ChargeState.NotCharging)
            {
                m_chargeState = ChargeState.Push;
                m_charge = 0.0f;
            }
        }

        if (m_chargeState != ChargeState.NotCharging)
        {
            m_charge = Mathf.Clamp01(m_charge + Time.deltaTime);
        }

        if (m_controller.GetButtonUp(Xbox360ControllerButtonId.RB))
        {
            if (m_chargeState == ChargeState.Pull)
            {
                FireHook();
                m_chargeState = ChargeState.NotCharging;
            }
        }

        if (m_controller.GetButtonUp(Xbox360ControllerButtonId.LB))
        {
            if (m_chargeState == ChargeState.Push)
            {
                FireHook();
                m_chargeState = ChargeState.NotCharging;
            }
        }

        if (m_rigidBody.position.y < -40.0f)
        {
            Destroy(this.gameObject);
        }

        var v = new Vector2(m_rigidBody.velocity.x, m_rigidBody.velocity.z);
        if (v.magnitude > 0.01f)
        {
            v.Normalize();
            m_aim = new Vector3(v.x, 0, v.y);
            transform.rotation = Quaternion.Euler(0, 180.0f - (180.0f / Mathf.PI) * Mathf.Atan2(v.y, v.x), 0);
        }

        m_chargeIndicator.localScale = Vector3.one * m_charge;
	}

    Vector3 m_aim = Vector3.right;

    void OnDestroy()
    {
        if (OnDeath != null)
        {
            OnDeath(this);
        }
    }

    void FireHook()
    {
        if (m_hook == null)
        {
            Vector3 vel = m_aim;
            vel.Normalize();
            vel *= m_hookSpeed;

            m_hook = (Hook)Instantiate(m_hookPrefab, this.transform.position, Quaternion.identity);
            m_hook.Init(vel, m_hookLifetime);
            m_hook.Id = Id;
            m_hook.Owner = this;
            m_hook.Power = (m_chargeState == ChargeState.Push ? 1.0f : -1.0f) * m_hookMaxPower * m_charge;
        }

        m_charge = 0.0f;
    }

    public void KnockOut(float time)
    {
        m_knockoutTimer = time;
    }
}
