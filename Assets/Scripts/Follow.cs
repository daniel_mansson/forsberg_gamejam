﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Follow : MonoBehaviour 
{
    public List<Transform> m_targets = new List<Transform>();
    public float m_moveFactor = 0.3f;
    public float m_moveSpeed = 3.0f;
    public Vector3 m_offset = Vector3.zero;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        var pos = transform.position;

        Vector3 target = Vector3.zero;
        int c = 0;

        m_targets.RemoveAll(t => t == null);

        foreach (var t in m_targets)
        {
            if(t != null)
            {
                target += t.position;
            }
        }

        if (c != 0)
        { 
            target /= c;
        }

        target += m_offset;

        pos.x = Mathf.Lerp(pos.x, target.x * m_moveFactor, Time.deltaTime * m_moveSpeed);
        pos.z = Mathf.Lerp(pos.z, target.z * m_moveFactor, Time.deltaTime * m_moveSpeed);

        transform.position = pos;
	}
}
