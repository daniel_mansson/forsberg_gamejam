﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour 
{
    public enum TileId
    { 
        Ground,
        Goal,
        Spawn
    }

    public TileId m_id = TileId.Ground;
    public int m_team = -1;
}
