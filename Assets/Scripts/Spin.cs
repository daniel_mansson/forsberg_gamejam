﻿using UnityEngine;
using System.Collections;

public class Spin : MonoBehaviour 
{
	public Vector3 axis;
	public float speed;

	void Update () 
	{
		transform.rotation = Quaternion.AngleAxis (speed * Time.time, axis);
	}
}
