﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelSelect : MonoBehaviour 
{
    public List<Texture2D> m_levelImages;
    public Texture2D Current { get; private set; }

    public static LevelSelect s_instance = null;

    void Awake()
    {
        if (s_instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            s_instance = this;
            Current = m_levelImages[0];
            DontDestroyOnLoad(this.gameObject);
        }
    }

	void Update () 
    {
        for (int i = 0; i < m_levelImages.Count; i++)
        {
            if (Input.GetKeyDown(KeyCode.Alpha0 + i))
            { 
                Current = m_levelImages[i];
                Application.LoadLevel(0);
            }
        }
	}
}
