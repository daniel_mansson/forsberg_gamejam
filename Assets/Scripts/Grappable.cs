﻿using UnityEngine;
using System.Collections;

public class Grappable : MonoBehaviour 
{

    public int Id { get; set; }

    void Awake()
    {
        Id = -1;
    }

	// Update is called once per frame
	void Update () 
    {
	
	}

    public bool IsOnGround()
    {
        var g = GetComponent<IsOnGround>();

        if (g != null)
            return g.OnGround;
        else
            return false;
    }
}
