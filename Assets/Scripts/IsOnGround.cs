﻿using UnityEngine;
using System.Collections;

public class IsOnGround : MonoBehaviour 
{
    public bool OnGround
    {
        get { return count > 0; }
    }

    int count = 0;

    void OnTriggerEnter(Collider c)
    {
        if(c.GetComponent<Tile>() != null)
            ++count;
    }

    void OnTriggerExit(Collider c)
    {
        if (c.GetComponent<Tile>() != null)
            --count;
    }
}
