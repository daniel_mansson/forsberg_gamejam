﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public LevelManager m_level;
    public Player m_playerPrefab;
    public Ball m_ballPrefab;
    public Follow m_cameraFollow;
    public Text m_infoText;

    public Color m_info;
    public Color m_team0;
    public Color m_team1;

    public Material m_mat0;
    public Material m_mat1;

    public int m_numBalls = 1;
    public float m_winTime = 5.0f;

    public AudioClip m_scoreSound;

    int m_score0 = 0;
    int m_score1 = 0;

    float m_scoreTimer = 3.0f;
    int m_scoringTeam = -1;

    InputManager m_input;
    List<PlayerEntry> m_players = new List<PlayerEntry>();
    List<BallEntry> m_balls = new List<BallEntry>();

    class PlayerEntry
    {
        public Controller controller;
        public Player currentPlayer;
        public float spawnTimer;
        public int team;
        public int id;
    }

    class BallEntry
    {
        public Ball ball;
        public float spawnTimer;
    }

    void Start()
    {
        m_infoText.text = "";

        m_input = GetComponent<InputManager>();

        int count = m_input.GetControllerCount();

        Debug.Log("Detected " + count + " players");

        for (int i = 0; i < count; i++)
        {
            var entry = new PlayerEntry();

            entry.controller = m_input.GetController(i);
            entry.spawnTimer = 2.0f;
            entry.team = i % 2;
            entry.id = i;

            m_players.Add(entry);
        }

        for (int i = 0; i < m_numBalls; i++)
        {
            var e = new BallEntry();

            e.spawnTimer = 2.0f;

            m_balls.Add(e);
        }
    }

    void Update()
    {
        foreach (var p in m_players)
        {
            if (p.currentPlayer == null)
            {
                p.spawnTimer -= Time.deltaTime;
                if (p.spawnTimer < 0.0f)
                {
                    p.spawnTimer = 2.0f;

                    var spawn = m_level.GetSpawns(p.team);
                    Vector3 pos = spawn[Random.Range(0, spawn.Count)];
                    p.currentPlayer = (Player)Instantiate(m_playerPrefab, pos, Quaternion.identity);
                    p.currentPlayer.m_controller = p.controller;
                    p.currentPlayer.Id = p.id;
                    p.currentPlayer.SetMaterial(p.team == 0 ? m_mat0 : m_mat1);

                    Debug.Log("Spawning player " + p.id);
                }
            }
        }

        int team0balls = 0;
        int team1balls = 0;
        foreach (var ball in m_balls)
        {
            if (ball.ball == null)
            {
                ball.spawnTimer -= Time.deltaTime;
                if (ball.spawnTimer < 0.0f)
                {
                    ball.spawnTimer = 3.0f;

                    var spawn = m_level.GetSpawns(-1);

                    Vector3 pos = spawn[Random.Range(0, spawn.Count)];
                    ball.ball = (Ball)Instantiate(m_ballPrefab, pos, Quaternion.identity);

                    m_cameraFollow.m_targets.Add(ball.ball.transform);
                }
            }
            else
            {
                if (ball.ball.TeamId == 0)
                    team0balls++;
                else if (ball.ball.TeamId == 1)
                    team1balls++;
            }
        }

        if (m_scoringTeam == -1)
        {
            if (team0balls == m_numBalls)
            {
                m_scoringTeam = 0;
                m_scoreTimer = m_winTime;
            }
            else if (team1balls == m_numBalls)
            {
                m_scoringTeam = 1;
                m_scoreTimer = m_winTime;
            }
        }
        else
        {
            if (team0balls != m_numBalls && team1balls != m_numBalls)
            {
                m_scoringTeam = -1;
                m_infoText.text = "";
            }
            else
            {
                m_infoText.color = (m_scoringTeam == 0 ? m_team0 : m_team1);
                m_infoText.text = (m_scoringTeam == 0 ? "Yellow" : "Purple") + " scores in " + m_scoreTimer.ToString("0.00");
            }

            m_scoreTimer -= Time.deltaTime;
            if (m_scoreTimer < 0)
            {
                Debug.Log("SCORE " + m_scoringTeam);
                AudioSource.PlayClipAtPoint(m_scoreSound, Camera.main.transform.position);
                StartCoroutine(ScoreText((m_scoringTeam == 0 ? "Yellow" : "Purple"), (m_scoringTeam == 0 ? m_team0 : m_team1)));

                m_scoringTeam = -1;

                if (m_scoringTeam == 0)
                    m_score0++;
                else
                    m_score1++;

                foreach (var p in m_players)
                {
                    Destroy(p.currentPlayer.gameObject);
                    p.spawnTimer = 8.0f;
                }

                foreach (var b in m_balls)
                {
                    Destroy(b.ball.gameObject);
                    b.spawnTimer = 5.0f;
                }
            }
        }
    }

    IEnumerator ScoreText(string team, Color color)
    {
        m_infoText.color = color;
        m_infoText.text = "Team " + team + " got a point!!";

        yield return new WaitForSeconds(4.0f);
        m_infoText.text = "";

        yield return new WaitForSeconds(1.0f);
        m_infoText.color = m_info;
        m_infoText.text = "Get ready!";

        yield return new WaitForSeconds(3.0f);
        m_infoText.text = "Go!";
        yield return new WaitForSeconds(0.6f);
        m_infoText.text = "";
    }
}