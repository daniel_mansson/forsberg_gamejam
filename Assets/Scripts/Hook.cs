﻿using UnityEngine;
using System.Collections;

public class Hook : MonoBehaviour 
{
    Rigidbody m_rigidBody;
    float m_timer;

    public int Id { get; set; }
    public Player Owner { get; set; }
    public float Power { get; set; }

    void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody>();
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
        m_timer -= Time.deltaTime;

        if (m_timer < 0)
        {
            Destroy(this.gameObject);
        }
	}

    void OnTriggerEnter(Collider c)
    {
        var p = c.gameObject.GetComponent<Grappable>();
        Debug.Log("HIT!");

        if (p != null && p.Id != Id)
        {
            Debug.Log("SUPERHIT!");

            Destroy(this.gameObject);

            var player = Owner.GetComponent<Rigidbody>();
            var other = p.GetComponent<Rigidbody>();

            Vector3 impulse = other.position - player.position;
            impulse.y = 0.0f;
            impulse.Normalize();
            impulse.y += 1.7f;
            impulse.Normalize();
            impulse.x *= Power;
            impulse.y *= Mathf.Abs(Power);
            impulse.z *= Power;

            player.AddForce(impulse, ForceMode.Impulse); ;
            impulse.x *= -1.0f;
            impulse.z *= -1.0f;
            other.AddForce(impulse, ForceMode.Impulse);

            Owner.KnockOut(0.1f);
            var op = other.GetComponent<Player>();
            if (op != null)
                op.KnockOut(1.0f);
            
            //TODO: tänk på onground
        }
    }

    public void Init(Vector3 vel, float lifetime)
    {
        m_rigidBody.velocity = vel;
        m_timer = lifetime;
    }
}
