﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour 
{
    [System.Serializable]
    public class TileLookup
    {
        public Color color;
        public GameObject tile;
    }

    public List<TileLookup> m_lookupTable;

    List<Vector3>[] m_spawns = new List<Vector3>[3];

    public List<Vector3> GetSpawns(int team)
    {
        Vector2 r = Random.insideUnitCircle;
        Vector3 offset = new Vector3(r.x, 0, r.y);
        var spawn = m_spawns[team + 1];
        return new List<Vector3>() { spawn[Random.Range(0, spawn.Count)] + offset };
    }

	void Start () 
    {
        BuildLevel();
	}

    [ContextMenu("Build")]
    public void BuildLevel()
    {
        Texture2D m_levelImage = LevelSelect.s_instance.Current;

        for (int i = 0; i < m_spawns.Length; i++)
        {
            m_spawns[i] = new List<Vector3>();        
        }

        float size = 3.0f;

        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        Dictionary<Color, GameObject> lookup = new Dictionary<Color,GameObject>();

        foreach (var t in m_lookupTable)
        {
            lookup.Add(t.color, t.tile);
        }

        for (int y = 0; y < m_levelImage.height; y++)
        {
            for (int x = 0; x < m_levelImage.width; x++)
            {
                Vector3 pos = new Vector3(x * size - size * m_levelImage.width * 0.5f, 0.0f, y * size - size * m_levelImage.height * 0.5f);

                Color c = m_levelImage.GetPixel(x, y);


                GameObject prefab = null;
                if(lookup.TryGetValue(c, out prefab))
                {
                    GameObject obj = (GameObject)Instantiate(prefab, pos, Quaternion.identity);

                    obj.transform.parent = transform;

                    var t = obj.GetComponentInChildren<Tile>();
                    if (t != null)
                    {
                        if(t.m_team != -1 || t.m_id == Tile.TileId.Spawn)
                            m_spawns[t.m_team + 1].Add(pos + Vector3.up * 3.0f);
                    }
                }
            }
        }
    }
	
	void Update () 
    {
	
	}
}
